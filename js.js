//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
jQuery(document).ready(function($){
// $(function() {
// $(document).ready(function(){	
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////	
	
	
	 $(".no-touch #scroller_2").niceScroll({cursorborder:"1px solid #323232",cursorcolor:"#323232",cursoropacitymin: 0.5,cursoropacitymax:1,cursorwidth: "10px",cursorborderradius: "0px",boxzoom:false,autohidemode: false,hwacceleration: true,enablekeyboard: true,touchbehavior:false,grabcursorenabled: true}); 
	 
	 $(".no-touch #scroller_3").niceScroll({cursorborder:"1px solid #323232",cursorcolor:"#323232",cursoropacitymin: 0.5,cursoropacitymax:1,cursorwidth: "10px",cursorborderradius: "0px",boxzoom:false,autohidemode: false,hwacceleration: true,enablekeyboard: true,touchbehavior:false,grabcursorenabled: true}); 
	

 	$("#scroller_4").mousewheel(function(event, delta) {
      this.scrollLeft -= (delta * 50);    
      event.preventDefault();
    });
	
	/* ********* */
	
	$("#scroller_5").mousewheel(function(event, delta) {
      this.scrollLeft -= (delta * 50);    
      event.preventDefault();
    });
	
	var screen_width = $(window).width(); // minus the default scrollbar = 15px -- change to innerWidth; if using custom/fancy scrollbars (vertical) for body/html etc.
	var item_width_percentage = 30; /* dont use % */
	var item_width = (screen_width * item_width_percentage) / 100;	
	var item_count = $("#scroller_5_inner_wrap .item").length;
	var inner_wrap_width = item_width * item_count; 
	$("#scroller_5_inner_wrap").css({'width' : inner_wrap_width + 'px'});
	$("#scroller_5_inner_wrap .item").css({'width' : item_width + 'px'});
	
	/* ********* */

	/* ********* */			
		
		var mywidth0 = $(window).width(); // minus the default scrollbar = 15px
		var myheight0 = $(window).height();
		var mywidth = window.innerWidth; // plus/including the default scrollbar = 15px
		var myheight = window.innerHeight;
		document.getElementById('mywidthdiv').innerHTML = mywidth0 + " / " + myheight0 + "<br>---<br>" + mywidth + " / " + myheight;
				
	/* ********* */
		
	//////////////////////////////////////////////////////////////////////////////////////////	
	// begin $(window).resize(function(){
	$(window).resize(function(){
		
		/* ********* */	
		
		var mywidth0 = $(window).width();
		var myheight0 = $(window).height();
		var mywidth = window.innerWidth;
		var myheight = window.innerHeight;
		document.getElementById('mywidthdiv').innerHTML = mywidth0 + " / " + myheight0 + "<br>---<br>" + mywidth + " / " + myheight;
		
		/* ********* */
		
	}); // end $(window).resize(function(){
	//////////////////////////////////////////////////////////////////////////////////////////	
	
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
}); // end jQuery(document).ready(function($){
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

